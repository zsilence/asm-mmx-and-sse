#include <windows.h>
#include <sstream>
#include <string>

#define DO_BUTTON_ID 127
#define ARR1_TXT_ID 128
#define ARR2_TXT_ID 129
#define ARR3_TXT_ID 130
#define ARR4_TXT_ID 131
#define ARR_OUT_TXT_ID 132
#define WND_CLASS_NAME L"window"

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void onDoClick(HWND hWnd);

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, INT iCmdShow)
{
    HWND hWnd;
    MSG msg;
    WNDCLASS wndClass;
    wndClass.style = CS_HREDRAW | CS_VREDRAW;
    wndClass.lpfnWndProc = WndProc;
    wndClass.cbClsExtra = 0;
    wndClass.cbWndExtra = 0;
    wndClass.hInstance = hInstance;
    wndClass.hIcon = NULL;
    wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    wndClass.hbrBackground = NULL;
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = WND_CLASS_NAME;
    RegisterClass(&wndClass);

    hWnd = CreateWindow(WND_CLASS_NAME, L"Window", WS_SYSMENU,
        CW_USEDEFAULT, CW_USEDEFAULT, 320, 320,
        NULL, NULL, hInstance, NULL);
	CreateWindow(TEXT("Edit"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 20, 260, 20, hWnd, (HMENU)ARR1_TXT_ID, NULL, NULL);
	CreateWindow(TEXT("Edit"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 60, 260, 20, hWnd, (HMENU)ARR2_TXT_ID, NULL, NULL);
	CreateWindow(TEXT("Edit"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 100, 260, 20, hWnd, (HMENU)ARR3_TXT_ID, NULL, NULL);
	CreateWindow(TEXT("Edit"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 140, 260, 20, hWnd, (HMENU)ARR4_TXT_ID, NULL, NULL);
	CreateWindow(TEXT("Edit"), TEXT(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		20, 180, 260, 20, hWnd, (HMENU)ARR_OUT_TXT_ID, NULL, NULL);

    CreateWindow(L"button", L"Calculate", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
        210, 240, 80, 30, hWnd, (HMENU)DO_BUTTON_ID, hInstance, NULL);

    ShowWindow(hWnd, iCmdShow);
    UpdateWindow(hWnd);

    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message,
    WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message)
    {
    case WM_INITDIALOG:
        return 0;
    case WM_COMMAND:
        switch (LOWORD(wParam)) {
        case DO_BUTTON_ID:
            onDoClick(hWnd);
            break;
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
}


void onDoClick(HWND hWnd) {
	signed __int8 a[8];
	signed __int8 b[8];
	signed __int8 c[8];
	signed __int16 d[8];
	signed __int16 f[8];

	int tmp;
	std::wstringstream wss;
	std::wstring ws;

	TCHAR buff[1024];

	HWND hWndArr1Edit = GetDlgItem(hWnd, ARR1_TXT_ID);
	HWND hWndArr2Edit = GetDlgItem(hWnd, ARR2_TXT_ID);
	HWND hWndArr3Edit = GetDlgItem(hWnd, ARR3_TXT_ID);
	HWND hWndArr4Edit = GetDlgItem(hWnd, ARR4_TXT_ID);
	HWND hWndArr5Edit = GetDlgItem(hWnd, ARR_OUT_TXT_ID);

	GetWindowText(hWndArr1Edit, buff, 1024);
	ws = std::wstring(buff);
	wss << ws;
	for (int i = 0; i < 8; ++i){
		wss >> tmp;
		a[i] = (signed __int8)tmp;
	}
	wss.str(std::wstring());
	wss.clear();

	GetWindowText(hWndArr2Edit, buff, 1024);
	ws = std::wstring(buff);
	wss << ws;
	for (int i = 0; i < 8; ++i) {
		wss >> tmp;
		b[i] = (signed __int8)tmp;
	}
	wss.str(std::wstring());
	wss.clear();

	GetWindowText(hWndArr3Edit, buff, 1024);
	ws = std::wstring(buff);
	wss << ws;
	for (int i = 0; i < 8; ++i) {
		wss >> tmp;
		c[i] = (signed __int8)tmp;
	}
	wss.str(std::wstring());
	wss.clear();

	GetWindowText(hWndArr4Edit, buff, 1024);
	ws = std::wstring(buff);
	wss << ws;
	for (int i = 0; i < 8; ++i)
		wss >> d[i];
	wss.str(std::wstring());
	wss.clear();


	__asm {

		pxor mm0, mm0
		movq mm7, a	
		punpcklbw mm0, mm7
		psraw mm0, 8

		pxor mm1, mm1
		movq mm7, b
		punpcklbw mm1, mm7
		psraw mm1, 8

		paddsw mm0, mm1

		pxor mm1, mm1
		movq mm7, c
		punpcklbw mm1, mm7
		psraw mm1, 8

		pmullw mm0, mm1

		movq mm1, d
		paddsw mm0, mm1
		movq f, mm0

		pxor mm0, mm0
		movq mm7, a
		punpckhbw mm0, mm7
		psraw mm0, 8

		pxor mm1, mm1
		movq mm7, b
		punpckhbw mm1, mm7
		psraw mm1, 8

		paddsw mm0, mm1

		pxor mm1, mm1
		movq mm7, c
		punpckhbw mm1, mm7
		psraw mm1, 8

		pmullw mm0, mm1

		movq mm1, d + 8
		paddsw mm0, mm1
		movq f + 8, mm0

		emms
	}


	for (int i = 0; i < 8; ++i) {
		wss << std::to_wstring(f[i]) << " ";
	}
	ws = wss.str();
	wss.str(std::wstring());
	wss.clear();
	for (int i = 0; i < ws.length(); ++i)
		buff[i] = ws[i];
	buff[ws.length()] = L'\0';
	SetWindowText(hWndArr5Edit, buff);
}